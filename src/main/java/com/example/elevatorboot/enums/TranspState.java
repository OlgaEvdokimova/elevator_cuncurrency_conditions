package com.example.elevatorboot.enums;

public enum TranspState {
    NOT_STARTED,
    IN_PROCESS,
    COMPLETED
}

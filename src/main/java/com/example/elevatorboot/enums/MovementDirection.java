package com.example.elevatorboot.enums;

public enum MovementDirection {
    UP,
    DOWN
}

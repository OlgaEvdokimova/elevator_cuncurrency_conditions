package com.example.elevatorboot.utils;

public class ConstantUtil {
    public static final int INITIAL_ID = 1;
    public static final int INITIAL_FLOOR = 1;
    public static final int FLOORS = 5;
    public static final int ELEVATOR_CAPACITY = 4;
    public static final int PASSENGERS = 20;
    public static final int ELEVATORS = 1;
    public static final String PASSENGER  = "PASSENGER";
    public static final String ELEVATOR  = "ELEVATOR";
    public static final int INITIAL_SUM = 0;
    public static final Long ELEVATOR_TIME_FOR_SLEEP_WHILE_WAITING_BOARDING = 50L;

}

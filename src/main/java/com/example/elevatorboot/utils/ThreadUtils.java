package com.example.elevatorboot.utils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ThreadUtils {
    public static void sleep(Long millis){
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            log.info(e.getMessage());
        }
    }
}

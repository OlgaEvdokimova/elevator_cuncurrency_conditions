package com.example.elevatorboot.utils;

import com.example.elevatorboot.factory.MyThreadFactory;
import com.example.elevatorboot.model.Building;
import com.example.elevatorboot.model.Elevator;
import com.example.elevatorboot.model.Floor;
import com.example.elevatorboot.model.Passenger;
import com.example.elevatorboot.task2.Controller;
import com.example.elevatorboot.task2.ElevatorTask;
import com.example.elevatorboot.task2.PassengerTask;
import com.example.elevatorboot.validate.ValidationResult;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.elevatorboot.utils.ConstantUtil.*;

@Slf4j
public class EmulationUtil {
    public static void doWork() {

        /* Elevator starts from the 1st floor */
        List<Floor> floors = getFloors();

        Elevator elevator = new Elevator();

        Building building = new Building(elevator, floors);

        List<Passenger> passengers = getPassengersOnTheEachFloor(floors);

        floors.forEach(floor -> log.info("Floor" + floor));

        Controller controller = new Controller(building, elevator, PASSENGERS);


        ThreadFactory passengersThreadFactory = MyThreadFactory.getThreadFactory(PASSENGER);
        ExecutorService passengerExecutor = Executors.newFixedThreadPool(PASSENGERS, passengersThreadFactory);
        passengers.forEach(passenger -> {
            PassengerTask passengerTask = new PassengerTask(passenger, controller);
            passengerExecutor.submit(passengerTask);
        });

        Lock elevatorLock = new ReentrantLock();

        ThreadFactory elevatorThreadFactory = MyThreadFactory.getThreadFactory(ELEVATOR);
        ElevatorTask elevatorTask = new ElevatorTask(elevator, controller, elevatorLock);
        ExecutorService elevatorExecutor = Executors.newFixedThreadPool(ELEVATORS, elevatorThreadFactory);
        elevatorExecutor.submit(elevatorTask);


        passengerExecutor.shutdown();
        elevatorExecutor.shutdown();
        try {
            passengerExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
            elevatorExecutor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        ValidationResult.validateResult(floors, elevator, PASSENGERS);

    }

    private static List<Passenger> getPassengersOnTheEachFloor(List<Floor> floors) {
        List<Passenger> passengers = IntStream
                .range(0, PASSENGERS)
                .mapToObj(Passenger::new)
                .collect(Collectors.toList());

        passengers.forEach(passenger -> floors.get(passenger.getCurrentFloor() - 1).getDeparturePassengers().offer(passenger));

        return passengers;
    }

    private static List<Floor> getFloors() {
       return IntStream.range(INITIAL_FLOOR, FLOORS + INITIAL_FLOOR)
                .mapToObj(Floor::new)
                .collect(Collectors.toList());
    }
}

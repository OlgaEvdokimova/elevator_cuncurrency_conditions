package com.example.elevatorboot.utils;

public class TemplateUtil {
    public static final String ARE_ALL_DISPATCH_CONTAINERS_EMPTY = "ALL DISPATCH CONTAINERS ARE EMPTY %s";
    public static final String IS_ELEVATOR_CONTAINER_EMPTY = "ELEVATOR CONTAINER IS EMPTY %s";
    public static final String ARE_ALL_STATUSES_COMPLETED = "ALL STATUSES ARE COMPLETED %s";
    public static final String WERE_ALL_PEOPLE_TRANSPORTED = "ALL PEOPLE WERE TRANSPORTED %s";

    public static final String PASSENGER_IS_COMPLETED = "PASSENGER %s IS COMPLETED";
    public static final String BOARDING_OF_PASSENGER = "Boarding passenger %s on floor %s";
    public static final String DEBOARDING_OF_PASSENGER = "Deboarding passenger %s on floor %s from elevator";
    public static final String SPOTS_COUNT_IN_ELEVATOR = "Spots count %s in elevator";

    public static final String LEFT_PASSENGERS = "There are %s passengers left";
    public static final String WAITING = "WAITING";
    public static final String ELEVATOR_IS_MOVING_UP = "Elevator is moving up";
    public static final String ELEVATOR_IS_MOVING_DOWN = "Elevator is moving down";
    public static final String ELEVATOR_ARRIVED_TO_THE_FLOOR = "Elevator arrived to the floor %s";
    public static final String ELEVATOR_IS_MOVING_FROM_THE_FLOOR = "Elevator is moving from the floor %s";


}

package com.example.elevatorboot.task2;

import com.example.elevatorboot.enums.TranspState;
import com.example.elevatorboot.model.Passenger;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data
public class PassengerTask implements Runnable {
    private final Passenger passenger;
    private final Controller controller;

    public PassengerTask(Passenger passenger, Controller controller) {
        this.passenger = passenger;
        this.controller = controller;
        passenger.setTranspState(TranspState.IN_PROCESS);
    }

    public void run() {

        controller.boardPassenger(passenger);
        controller.unBoardPassenger(passenger);

    }
}

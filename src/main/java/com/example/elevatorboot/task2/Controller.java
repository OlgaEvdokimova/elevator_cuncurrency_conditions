package com.example.elevatorboot.task2;

import com.example.elevatorboot.enums.TranspState;
import com.example.elevatorboot.model.Building;
import com.example.elevatorboot.model.Elevator;
import com.example.elevatorboot.model.Floor;
import com.example.elevatorboot.model.Passenger;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.elevatorboot.utils.ConstantUtil.FLOORS;
import static com.example.elevatorboot.utils.TemplateUtil.*;

@Slf4j
@Data
public class Controller {

    private Building building;
    private Elevator elevator;
    private LocalDateTime currentTimePressingButton;
    private Object dateLock = new Object();
    private boolean isDoorOpen;
    private int passengerCount;
    private Lock lock;
    private final List<Condition> floorPassengersConditionsList;
    private final List<Condition> elevatorPassengersConditionsList;

    public Controller(Building building, Elevator elevator, int passengerCount) {
        this.building = building;
        this.elevator = elevator;
        this.isDoorOpen = false;
        this.passengerCount = passengerCount;
        this.lock = new ReentrantLock();
        this.floorPassengersConditionsList = getConditions();
        this.elevatorPassengersConditionsList = getConditions();
    }

    private List<Condition> getConditions() {
        return IntStream
                .range(0, FLOORS)
                .mapToObj(x -> lock.newCondition())
                .collect(Collectors.toList());
    }

    public void pressButton() {
        currentTimePressingButton = LocalDateTime.now();

    }

    public void arrivedToFloor() {
        currentTimePressingButton = LocalDateTime.now();
        isDoorOpen = true;
        unboardFromElevator();
        boardToElevator();
    }

    public void departureFrom() {
        isDoorOpen = false;
    }

    public boolean canMove() {
        return LocalDateTime.now().isAfter(currentTimePressingButton.plusSeconds(1));

    }


    public void boardPassenger(Passenger passenger) {
        try {
            lock.lock();

            while (!(passenger.getCurrentFloor() == elevator.getCurrentFloor() && isDoorOpen
                    && hasElevatorAvailableSpace())) {
                floorPassengersConditionsList.get(passenger.getCurrentFloor() - 1).await();
            }
            log.info(String.format(SPOTS_COUNT_IN_ELEVATOR, spotsCountInElevator()));
            pressButton();
            Floor currentFloor = getCurrentFloor();
            currentFloor.getDeparturePassengers().remove(passenger);
            elevator.getContainer().add(passenger);

            log.info(String.format(BOARDING_OF_PASSENGER, passenger.getId(), currentFloor.getFloorNumber()));

            passenger.setTranspState(TranspState.IN_PROCESS);
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        } finally {
            lock.unlock();
        }
    }

    public void unBoardPassenger(Passenger passenger) {
        try {
            lock.lock();

            while (!(passenger.getTargetFloor() == elevator.getCurrentFloor() && isDoorOpen)) {
                elevatorPassengersConditionsList.get(passenger.getTargetFloor() - 1).await();
            }
            pressButton();
            Floor currentFloor = getCurrentFloor();
            currentFloor.getArrivalPassengers().add(passenger);
            elevator.getContainer().remove(passenger);

            log.info(String.format(DEBOARDING_OF_PASSENGER, passenger.getId(), currentFloor.getFloorNumber()));

            passenger.setTranspState(TranspState.COMPLETED);

            log.info(String.format(PASSENGER_IS_COMPLETED, passenger.getId()));

            passengerCount--;

            log.info(String.format(LEFT_PASSENGERS, passengerCount));
        } catch (InterruptedException e) {
            log.error(e.getMessage());
        } finally {
            lock.unlock();
        }
    }


    public void boardToElevator() {
        lock.lock();
        try {
            floorPassengersConditionsList.get(elevator.getCurrentFloor() - 1).signalAll();
        } finally {
            lock.unlock();
        }
    }


    public void unboardFromElevator() {
        lock.lock();
        try {
            elevatorPassengersConditionsList.get(elevator.getCurrentFloor() - 1).signalAll();
        } finally {
            lock.unlock();
        }
    }

    private int spotsCountInElevator() {
        return elevator.getCapacity() - elevator.getContainer().size();
    }

    private Floor getCurrentFloor() {
        return building.getFloors().get(elevator.getCurrentFloor() - 1);
    }

    public boolean hasElevatorAvailableSpace() {
        return elevator.getContainer().size() < elevator.getCapacity();
    }
}

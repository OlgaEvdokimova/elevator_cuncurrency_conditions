package com.example.elevatorboot.task2;

import com.example.elevatorboot.model.Elevator;
import com.example.elevatorboot.utils.ThreadUtils;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.locks.Lock;

import static com.example.elevatorboot.utils.ConstantUtil.ELEVATOR_TIME_FOR_SLEEP_WHILE_WAITING_BOARDING;
import static com.example.elevatorboot.utils.TemplateUtil.*;

@Slf4j
@Data
public class ElevatorTask implements Runnable {

    private Elevator elevator;
    private Controller controller;


    public ElevatorTask(Elevator elevator, Controller controller, Lock lock) {
        this.elevator = elevator;
        this.controller = controller;
    }

    public void run() {
        while (controller.getPassengerCount() != 0) {

            log.info(String.format(ELEVATOR_ARRIVED_TO_THE_FLOOR, elevator.getCurrentFloor()));

            controller.arrivedToFloor();

            while (!controller.canMove()) {
                ThreadUtils.sleep(ELEVATOR_TIME_FOR_SLEEP_WHILE_WAITING_BOARDING);
                log.info(WAITING);
            }

            controller.departureFrom();

            log.info(String.format(ELEVATOR_IS_MOVING_FROM_THE_FLOOR, elevator.getCurrentFloor()));

            elevator.move();

        }
    }
}




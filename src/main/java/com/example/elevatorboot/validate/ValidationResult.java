package com.example.elevatorboot.validate;

import com.example.elevatorboot.utils.ConstantUtil;
import com.example.elevatorboot.utils.TemplateUtil;
import com.example.elevatorboot.enums.TranspState;
import com.example.elevatorboot.model.Elevator;
import com.example.elevatorboot.model.Floor;
import com.example.elevatorboot.model.Passenger;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
@Slf4j
public class ValidationResult {

    public static void validateResult(List<Floor> floors, Elevator elevator, int passengersNumber) {
        areAllDispatchContainersEmpty(floors);
        areElevatorContainersEmpty(elevator);
        areAllStatusesCompleted(floors);
        areAllPeopleWereTransported(floors, passengersNumber);
    }

    private static void areAllDispatchContainersEmpty(List<Floor> floors) {
        boolean isAllContainersEmpty = floors
                .stream()
                .map(floor -> floor.getDeparturePassengers().size())
                .noneMatch(size -> size > 0);

        log.info(String.format(TemplateUtil.ARE_ALL_DISPATCH_CONTAINERS_EMPTY, isAllContainersEmpty));
    }
    private static void areElevatorContainersEmpty(Elevator elevator) {
        boolean isElevatorContainerEmpty =  elevator.getContainer().size() == 0;

        log.info(String.format(TemplateUtil.IS_ELEVATOR_CONTAINER_EMPTY, isElevatorContainerEmpty));
    }

    private static void areAllStatusesCompleted(List<Floor> floors) {
        boolean isAllStatusesIsCompleted = floors
                .stream()
                .map(Floor::getArrivalPassengers)
                .allMatch(floor -> floor.stream()
                        .map(Passenger::getTranspState)
                        .allMatch(state -> state == TranspState.COMPLETED)
                );

        log.info(String.format(TemplateUtil.ARE_ALL_STATUSES_COMPLETED, isAllStatusesIsCompleted));
    }


    private static void areAllPeopleWereTransported(List<Floor> floors, int passengersNumber) {
        boolean areAllPeopleWereTransported = floors
                .stream()
                .map(floor -> floor.getArrivalPassengers().size())
                .reduce(ConstantUtil.INITIAL_SUM, Integer::sum) == passengersNumber;

        log.info(String.format(TemplateUtil.WERE_ALL_PEOPLE_TRANSPORTED, areAllPeopleWereTransported));
    }
}

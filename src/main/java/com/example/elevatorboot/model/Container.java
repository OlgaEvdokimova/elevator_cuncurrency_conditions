package com.example.elevatorboot.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class Container {
    private final List<Passenger> passengersContainer = new ArrayList<>();

    public void add(Passenger passenger){
        passengersContainer.add(passenger);
    }
    public void remove(Passenger passenger){
        passengersContainer.remove(passenger);
    }
    public int size(){
        return passengersContainer.size();
    }
}

package com.example.elevatorboot.model;

import com.example.elevatorboot.enums.TranspState;
import lombok.Data;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.elevatorboot.utils.ConstantUtil.FLOORS;
import static com.example.elevatorboot.utils.ConstantUtil.INITIAL_FLOOR;


@Data
public class Passenger {
    private Integer id;
    private int currentFloor;
    private int targetFloor;
    private TranspState transpState;

    public Passenger(Integer id) {
        List<Integer> floorsList = getRandomFloor();
        this.id = id;
        this.currentFloor = floorsList.get(0);
        this.targetFloor = floorsList.get(floorsList.size() -1);
        this.transpState = TranspState.NOT_STARTED;
    }

    private List<Integer> getRandomFloor() {
        List<Integer> floorsList = IntStream.range(INITIAL_FLOOR, FLOORS + 1)
                .boxed()
                .collect(Collectors.toList());
        Collections.shuffle(floorsList);
        return floorsList;
    }
}

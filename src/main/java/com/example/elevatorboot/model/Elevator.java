package com.example.elevatorboot.model;

import com.example.elevatorboot.enums.MovementDirection;
import com.example.elevatorboot.task2.Controller;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import static com.example.elevatorboot.utils.ConstantUtil.*;
import static com.example.elevatorboot.utils.TemplateUtil.ELEVATOR_IS_MOVING_DOWN;
import static com.example.elevatorboot.utils.TemplateUtil.ELEVATOR_IS_MOVING_UP;

@Slf4j
@Data
public class Elevator {
    private int capacity;
    private Container container;
    private int id;
    private int currentFloor;
    private int topFloor;
    private MovementDirection movementDirection;
    private Controller controller;

    public Elevator() {
        this.id = INITIAL_ID;
        this.capacity = ELEVATOR_CAPACITY;
        this.currentFloor = INITIAL_FLOOR;
        this.container = new Container();
        this.topFloor = FLOORS;
        this.movementDirection = MovementDirection.UP;

    }

    public void move() {
            if (currentFloor == INITIAL_FLOOR) {
                this.setMovementDirection(MovementDirection.UP);
            }

            if (currentFloor == FLOORS) {
                this.setMovementDirection(MovementDirection.DOWN);
            }

            if (movementDirection.equals(MovementDirection.UP)) {
                goUp();
                log.info(ELEVATOR_IS_MOVING_UP);
            } else {
                goDown();
                log.info(ELEVATOR_IS_MOVING_DOWN);
            }
    }

    public void goUp() {
        currentFloor++;
    }

    public void goDown() {
        currentFloor--;
    }


}

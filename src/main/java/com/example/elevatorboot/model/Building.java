package com.example.elevatorboot.model;

import lombok.Data;

import java.util.List;

@Data
public class Building {
    private Elevator elevator;
    private List<Floor> floors;

    public Building(Elevator elevator, List<Floor> floors) {
        this.elevator = elevator;
        this.floors = floors;
    }
}

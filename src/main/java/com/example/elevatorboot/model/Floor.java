package com.example.elevatorboot.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Data
public class Floor {

    private int floorNumber;
    private BlockingQueue<Passenger> departurePassengers;
    private List<Passenger> arrivalPassengers;

    public Floor(int floorNumber) {
        this.floorNumber = floorNumber;
        this.departurePassengers = new ArrayBlockingQueue<>(4);
        this.arrivalPassengers = new ArrayList<>();
    }

}

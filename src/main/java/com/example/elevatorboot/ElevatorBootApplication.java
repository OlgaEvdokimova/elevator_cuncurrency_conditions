package com.example.elevatorboot;

import com.example.elevatorboot.utils.EmulationUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class ElevatorBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ElevatorBootApplication.class, args);
        EmulationUtil.doWork();

    }

}
